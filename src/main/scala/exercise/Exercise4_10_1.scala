package exercise

import cats.Monad
import scala.annotation.tailrec

object Exercise4_10_1 {

    sealed trait Tree[+A]

    final case class Branch[A](left: Tree[A], right: Tree[A])
        extends Tree[A]

    final case class Leaf[A](value: A) extends Tree[A]

    def branch[A](left: Tree[A], right: Tree[A]): Tree[A] =
        Branch(left, right)

    def leaf[A](value: A): Tree[A] =
        Leaf(value)


    val example: Tree[Int] =
        Branch(
            Branch(
                Leaf(1),
                Leaf(2)),
            Leaf(3))


    val treeMonad: Monad[Tree] = new Monad[Tree] {
        def pure[A](value: A): Tree[A] =
            Leaf(value)

        def flatMap[A, B](t: Tree[A])(f: A => Tree[B]): Tree[B] =
            t match {
                case Leaf(v) => f(v)
                case Branch(left, right) =>
                    Branch(flatMap(left)(f), flatMap(right)(f))
            }

        def tailRecM[A, B](a: A)(f: A => Tree[Either[A, B]]): Tree[B] =
            f(a) match {
                case Leaf(Left(va)) => tailRecM(va)(f)
                case Leaf(Right(vb)) => Leaf(vb)
                case Branch(l, r) =>
                    Branch(flatMap(l) {
                        case Left(x) => tailRecM(x)(f)
                        case Right(x) => pure(x)
                    },
                    flatMap(r) {
                        case Left(y) => tailRecM(y)(f)
                        case Right(y) => pure(y)
                } // Jeezuz. Denne er rippa rett fra boka.

            )}
    }
}
