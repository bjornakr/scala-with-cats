package exercise

import cats.Functor
import cats.implicits._ // The book never mentions this... Why is it needed?

object Exercise3_5_4 {
    sealed trait Tree[+A]
    final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
    final case class Leaf[A](value: A) extends Tree[A]

    case class Zerg[A](value: A)

    object Zerg {
        implicit val zergFunctor = new Functor[Zerg] {
            override def map[A, B](zerg: Zerg[A])(f: A => B): Zerg[B] =
                Zerg(f(zerg.value))
        }
    }

    object Tree {
        def branch[A](left: Tree[A], right: Tree[A]): Tree[A] =
            Branch(left, right)

        def leaf[A](value: A): Tree[A] =
            Leaf(value)

        // implicit val treeFunctor: Functor[Tree] =
        //     new Functor[Tree] {
        //         def map[A, B](tree: Tree[A])(f: A => B): Tree[B] =
        //             tree match {
        //                 case Leaf(v) => leaf(f(v))
        //                 case Branch(left, right) => branch(map(left)(f), map(right)(f))
        //             }
        //     }


    }

        implicit def treeFunctor: Functor[Tree] =
            new Functor[Tree] {
                def map[A, B](tree: Tree[A])(func: A => B): Tree[B] =
                    tree match {
                        case Branch(left, right) =>
                            Branch(map(left)(func), map(right)(func))
                        case Leaf(value) =>
                            Leaf(func(value))
                    }
            }

    object Test {
        // import Tree._
        val a = Tree.leaf(1).map(_ * 10)
        val intTree = Tree.branch(Tree.leaf(1), Tree.leaf(2))
        val timesTenTree = intTree.map(_ * 10)
        Zerg(1).map(a => a * 10)
    }
}
