package exercise

object Exercise2_3 {

    trait Semigroup[A] {
        def combine(x: A, y: A): A
    }

    trait Monoid[A] extends Semigroup[A] {
        def empty: A
    }

    object Monoid {
        def apply[A](implicit monoid: Monoid[A]): Monoid[A] = monoid
    }

    implicit val boolWithAndMonoid =
        new Monoid[Boolean] {
        override def empty = true
        override def combine(a: Boolean, b: Boolean) = a && b
        }

    implicit val boolWithOrMonoid =
        new Monoid[Boolean] {
        override def empty = false
        override def combine(a: Boolean, b: Boolean) = a || b
        }

    implicit val boolWithXorMonoid =
        new Monoid[Boolean] {
        override def empty = false
        override def combine(a: Boolean, b: Boolean) = a ^ b
        }

    implicit val boolWithEqMonoid =
        new Monoid[Boolean] {
        override def empty = true
        override def combine(a: Boolean, b: Boolean) = a == b
    }

    // ===== Exercise2_4 =====

    implicit def setWithUnionMonoid[A]: Monoid[Set[A]] =
        new Monoid[Set[A]] {
            override def empty = Set.empty
            override def combine(a: Set[A], b: Set[A]) = a.union(b)
    }

    implicit def setWithIntersectSemigroup[A]: Semigroup[Set[A]] =
        new Semigroup[Set[A]] {
            override def combine(a: Set[A], b: Set[A]) = a.intersect(b)
    }

    // implicit class MonoidOps[A](value: A) {
    //   def combine(value2: A)(implicit monoid: Monoid[A]) =
    //       monoid.combine(value, value2)
    // }

    object Testing {
        // val a = true.combine(false)
    }
}
