package exercise

import cats.Monoid
import cats.instances.int._ // for Monoid
import cats.instances.option._ // for Monoid
import cats.syntax.semigroup._ // for |+|

object Exercise2_5_4 {
    // def add(items: List[Int]): Int =
    // items match {
    // case List() => Monoid[Int].empty
    // case (x::xs) => x |+| add(xs)
    // }

    def addFold(items: List[Int]): Int =
        items.foldLeft(Monoid[Int].empty){ (acc, i) => acc |+| i }

    def addMonoid[A](items: List[A])(implicit moniod: Monoid[A]): A =
        items.foldLeft(moniod.empty){ (acc, i) => acc |+| i }

    case class Order(totalCost: Double, quantity: Double)

    object Order {
        implicit val orderMonoid =
            new Monoid[Order] {
                override def empty = Order(0, 0)
                override def combine(a: Order, b: Order) =
                    Order(a.totalCost + b.totalCost, a.quantity + b.quantity)
        }
    }

    object Testing {
        val l = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
        // val a = add(l)
        val b = addFold(l)
        val c = addMonoid(List(Some(1), None, Some(3), None))
        Order(1.0, 1.0).combine(Order(2.0, 2.0))
    }
}
