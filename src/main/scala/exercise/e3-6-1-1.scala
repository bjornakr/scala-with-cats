package exercise

class Exercise3_6_1_1 {

    trait Printable[A] {
        self =>

        def pformat(value: A): String

        def contramap[B](func: B => A): Printable[B] =
            new Printable[B] {
                def pformat(value: B): String =
                    self.pformat(func(value))
            }
    }

    def format[A](value: A)(implicit p: Printable[A]): String =
        p.pformat(value)

    case class Box[A](value: A)

    // object Printable {

    implicit val stringPrintable: Printable[String] =
        new Printable[String] {
            def pformat(value: String): String =
                "\"" + value + "\""
        }

    implicit val booleanPrintable: Printable[Boolean] =
        new Printable[Boolean] {
            def pformat(value: Boolean): String =
                if(value) "yes" else "no"
        }

    // implicit def boxPrintable[A](implicit p: Printable[A]): Printable[Box[A]] =
    //     new Printable[Box[A]] {
    //         def pformat(box: Box[A]): String =
    //             p.pformat(box.value)
    //     }

    implicit def boxPrintable[A](implicit p: Printable[A]): Printable[Box[A]] =
        p.contramap[Box[A]](_.value)
// }

    implicit class PrintableOps[A](value: A) {
        def pformat(implicit printable: Printable[A]): String = {
            printable.pformat(value)
        }
        def print(implicit printable: Printable[A]): Unit = {
            println(printable.pformat(value))
        }
    }

    format(Box("A"))

    object Testing {
        // import Printable._

        format("A")
        format(Box("A"))
        format(Box(false))
    }
}
