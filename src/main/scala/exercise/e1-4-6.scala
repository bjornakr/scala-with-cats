package exercise

import cats._
import cats.instances.int._
import cats.instances.string._
import cats.syntax.show._ // for show

object Exercise1_4_6 {
    // implicit val stringShow: Show[String] = Show.show(s => s)
    // implicit val intShow: Show[Int] = Show.show(_.toString)
    implicit val catShow: Show[Cat] = {
        Show.show(cat => {
            val name = cat.name.show
            val age = cat.age.show
            val color = cat.color.show
            s"${name} is a ${age} year-old ${color} cat."
            })
    }

    // Testing
    object Testing {
        val s = "This is a string.".show
        val i = 123.show
        val c = Cat("Harry", 3, "blue").show
    }

}
