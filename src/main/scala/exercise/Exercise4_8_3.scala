package exercise

import cats.data.Reader
import cats.syntax.applicative._ // for pure

object Exercise4_8_3 {
    type DbReader[A] = Reader[Db, A]

    case class Db(
        usernames: Map[Int, String],
        passwords: Map[String, String]
    )

    def findUsername(userId: Int): DbReader[Option[String]] =
        Reader(db => db.usernames.get(userId))

    def checkPassword(username: String, password: String): DbReader[Boolean] =
        Reader(db => db.passwords.get(username).contains(password))

    def checkLogin(userId: Int, password: String): DbReader[Boolean] =
        for {
            username <- findUsername(userId)
            isValidPassword <- username.map {
                                    u => checkPassword(u, password)
                                }.getOrElse {
                                    false.pure[DbReader]
                                }
        } yield isValidPassword
}
