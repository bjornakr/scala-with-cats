package exercise

import cats.Semigroupal
import cats.instances.option._ // for Semigroupal
import cats.syntax.apply._ // for tupled and mapN
import cats.Monad
import cats.syntax.flatMap._ // for flatMap
import cats.syntax.functor._ // for map

object Exercise6_3_1_1 {
    // testing combine
    val test: Option[(Int, String)] = Semigroupal[Option].product(Some(123), Some("abc"))

    val test2: Option[(Int, Int, Int)] = Semigroupal.tuple3(Option(1), Option(2), Option.empty[Int])

    val test3: Option[Int] = Semigroupal.map3(Option(1), Option(2), Option.empty[Int])(_ + _ + _)

    val test4: Option[(Int, String)] = (Option(123), Option("abc")).tupled

    case class Zerg(name: String, health: Int)

    val test5: Option[Zerg] = (Option("Roger"), Option(199)).mapN(Zerg)


    def product[M[_]: Monad, A, B](x: M[A], y: M[B]): M[(A, B)] =
        x.flatMap(a => y.map(b => (a, b)))

    def prodopt(x: Option[Int], y: Option[String]): Option[(Int, String)] =
        for {
            i <- x
            s <- y
        } yield (i, s)

//        x.flatMap(i => y.map(s => (i, s)))


}

object Validator {
    import cats.data.Validated
    import cats.instances.list._ // for Monoid
    import cats.syntax.validated._ // <obj>.valid, <obj>.invalid

    import cats.syntax.applicative._ // for pure
    import cats.syntax.applicativeError._ // for raiseError

    type Errors = List[String]

    val a0 = Validated.Valid(123)
    val a1 = Validated.Invalid(List("Error 1"))

    val a2 = Validated.valid[Errors, Int](123)
    val a3 = Validated.invalid[Errors, Int](List("Mothercracker!"))

    val a4 = 123.valid
    val a5 = List("Mothrkraka").invalid

    type ErrorsOr[A] = Validated[Errors, A]

    val a6 = 123.pure[ErrorsOr]
    val a7 = List("Kraka").raiseError[ErrorsOr, Int]


}
