package exercise

import cats.Eval
import cats.data.{IndexedStateT, State}
import cats.syntax.applicative._ // for pure

object Exercise4_9_3 {
    type CalcState[A] = State[List[Int], A]

    sealed trait Ops
    case object Plus extends Ops
    case object Minus extends Ops
    case object Muliply extends Ops
    case object Divide extends Ops



    def evalOne(symbol: String): CalcState[Int] = {
        State[List[Int], Int] { intList =>
            symbol match {
                case "+" => {
                    val sum = intList.sum
                    (List(sum), sum)
                }
                case "*" => {
                    val product = intList.product
                    (List(product), product)
                }
                case iStr => {
                    val i = iStr.toInt
                    (i.toInt :: intList, i)
                }
            }
        }
    }

    val program: IndexedStateT[Eval, List[Int], List[Int], Int] = for {
        _ <- evalOne("1")
        _ <- evalOne("2")
        ans <- evalOne("+")
    } yield ans

    def evalAll(postOrderCalcs: List[String]): CalcState[Int] = {
        val z: CalcState[Int] = 0.pure[CalcState]  // TODO: Jeg er ikke 100% komfortabel med "pure"
//        val z2: CalcState[Int] = State[List[Int], Int] { intList =>
//            (Nil, 0)
//        }

        def loop(ss: List[String]): CalcState[Int] = {
            ss match {
                case Nil => z
                case e::es => {
                    loop(es).flatMap(_ => evalOne(e))
                }
            }
        }

        loop(postOrderCalcs.reverse)
    }


    def evalAllSolution(input: List[String]): CalcState[Int] =
        input.foldLeft(0.pure[CalcState]) { (a: CalcState[Int], b: String) =>
            a.flatMap(_ => evalOne(b))
        }

    def evalInput(s: String): Int = {
        val symbols = s.split(" ").toList
        evalAll(symbols).runA(Nil).value
    }
}
