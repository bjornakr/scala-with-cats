package exercise

// To test in REPL:
// import exercise.Exercise1_3._, PrintableInstances._, PrintableSyntax._

object Exercise1_3 {

    trait Printable[A] {
        def pformat(a: A): String
    }

    object PrintableInstances {
        implicit val stringPrintable: Printable[String] =
            new Printable[String] {
                def pformat(a: String): String =
                    a.toString
            }

        implicit val intPrintable: Printable[Int] =
            new Printable[Int] {
                def pformat(a: Int): String =
                    a.toString
            }

        implicit val catPrintable: Printable[Cat] =
            new Printable[Cat] {
                def pformat(a: Cat): String =
                    s"${a.name} is a ${a.age} year-old ${a.color} cat."
            }
    }

    // Replaced by "PrintableSyntax".
    // object Printable {
    //  def pformat[A](a: A)(implicit printable: Printable[A]): String =
    //      printable.pformat(a)

    //  def print[A](a: A)(implicit printable: Printable[A]): Unit =
    //      println(printable.pformat(a))
    // }

    // USAGE: 123.pformat, Cat("Bob", 3, "green").pformat
    object PrintableSyntax {
        implicit class PrintableOps[A](value: A) {
            def pformat(implicit printable: Printable[A]): String = {
                printable.pformat(value)
            }
            def print(implicit printable: Printable[A]): Unit = {
                println(printable.pformat(value))
            }
        }
    }
}
