package exercise

object Exercise3_6_2_1 {

    trait Codec[A] {
        def encode(value: A): String
        def decode(value: String): A
        def imap[B](dec: A => B, enc: B => A): Codec[B] = {
            val self = this
            new Codec[B] {
                def encode(value: B): String = self.encode(enc(value))
                def decode(value: String): B = dec(self.decode(value))
            }
        }
    }

    def encode[A](value: A)(implicit c: Codec[A]): String =
        c.encode(value)

    def decode[A](value: String)(implicit c: Codec[A]): A =
        c.decode(value)

    implicit val stringCodec: Codec[String] =
        new Codec[String] {
            def encode(value: String): String = value
            def decode(value: String): String = value
        }

    implicit val intCodec: Codec[Int] =
        new Codec[Int] {
            def encode(value: Int): String = value.toString
            def decode(value: String): Int = value.toInt
        }

    implicit val intCodec2: Codec[Int] =
        stringCodec.imap(_.toInt, _.toString)

    implicit val booleanCodec: Codec[Boolean] =
        stringCodec.imap(_.toBoolean, _.toString)

    implicit val doubleCodec: Codec[Double] =
        stringCodec.imap(_.toDouble, _.toString)

    case class Box[A](value: A)

    implicit def boxCodec[A](implicit c: Codec[A]): Codec[Box[A]] =
        c.imap[Box[A]](Box(_), _.value)

    object Testing {
        val t0 = encode[String]("Hei_enc")
        val t1 = decode[String]("Hei_dec")
        val t2 = encode(true)
        val t3 = decode[Boolean]("false")
        val t4 = encode(1.0)
        val t5 = decode[Double]("1.0")
        val t6 = encode(Box(true))
        val t7 = decode[Box[Boolean]]("false")

    }

}
