package exercise

import cats.data.EitherT
import cats.instances.future._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object Exercise5_4 {

    // type Response[A] = Future[Either[String, A]]

    type Response[A] = EitherT[Future, String, A]

    def getPowerLevel(autobot: String): Response[Int] = {
        powerLevels.get(autobot) match {
            case None => EitherT.left(Future(s"No such autobot: $autobot."))
            case Some(a) => EitherT.right(Future(a))
        }
    }

    val powerLevels = Map(
        "Jazz"      -> 6,
        "Bumblebee" -> 8,
        "Hot rod"   -> 10
    )

    def canSpecialMove(ally1: String, ally2: String): Response[Boolean] = {
        for {
            pow1 <- getPowerLevel(ally1)
            pow2 <- getPowerLevel(ally2)
        } yield pow1 + pow2 > 15
    }

    def tacticalReport(ally1: String, ally2: String): String = {
        val result = canSpecialMove(ally1, ally2)
        val r01: Future[Either[String, Boolean]] = result.value
        val r02: Either[String, Boolean] = Await.result(r01, 3.seconds)
        r02 match {
            case Left(err) => s"Comms error: $err"
            case Right(true) => s"$ally1 and $ally2 are ready to roll out!"
            case Right(false) => s"$ally1 and $ally2 need a recharge."
        }
    }
}
