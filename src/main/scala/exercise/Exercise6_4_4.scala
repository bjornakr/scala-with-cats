package exercise

import cats.data.Validated
import scala.util.Try
import cats.syntax.either._ // for catchOnly
import cats.instances.list._ // for Semigroupal
import cats.syntax.apply._ // for mapN

object Exercise6_4_4 {
    type RequestData = Map[String, String]
    type ValidationResult[A] = Either[List[String], A]

    case class User(name: String, age: Int)

    def readName(r: RequestData): ValidationResult[String] = {
        val errorMessage = "Name is blank"
        val a = r.getOrElse("name", "").trim
        if (a.isEmpty)
            Left(List(errorMessage))
        else
            Right(a)
    }

    def readAge(r: RequestData): ValidationResult[Int] = {
        val errorMessage = "Age must be a non-negative integer."
        val a = r.getOrElse("age", "-1")
        val age = Try(a.toInt).toOption.getOrElse(-1)
        if (age < 0)
            Left(List(errorMessage))
        else
            Right(age)
    }

    def getValue(key: String)(r: RequestData): ValidationResult[String] =
        r.get(key).toRight(List(s"Key missing: '$key'."))

    def getName(r: RequestData) = getValue("name")(r)
    def getAge(r: RequestData) = getValue("age")(r)

    def parseInt(i: String): ValidationResult[Int] =
        Try(i.toInt)
            .toEither
            .leftMap(_ => List(s"Could not parse int: '$i'"))

    def pi(s: String): ValidationResult[Int] = {
        Either.catchOnly[NumberFormatException](s.toInt)
          .leftMap(_ => List(s"Could not parse int: '$s'"))
    }

    def nonBlank(s: String): ValidationResult[String] = {
        if (s.isEmpty) Left(List("Something is empty")) else Right(s)
    }

    def nonNegative(i: Int): ValidationResult[Int] = {
        if (i < 0) Left(List("Negative")) else Right(i)
    }

    def rName(r: RequestData): ValidationResult[String] =
        for {
            r0 <- getName(r)
            r1 <- nonBlank(r0)
        } yield r1

    def rAge(r: RequestData): ValidationResult[Int] =
        for {
            r0 <- getAge(r)
            r1 <- parseInt(r0)
            r2 <- nonNegative(r1)
        } yield r2

    def createUser(r: RequestData): Validated[List[String], User] =
        (rName(r).toValidated,
            rAge(r).toValidated)
            .mapN(User.apply(_ ,_))

}
