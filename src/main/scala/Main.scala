package com.example
import exercise.Cat
import exercise.Exercise1_3.PrintableInstances._
import exercise.Exercise1_3.PrintableSyntax._

object Main {
    def main(args: Array[String]): Unit = {
        123.pformat
        Cat("Harry Flux", 10, "blue").pformat
        println("Hello, pformat!")
    }
}
