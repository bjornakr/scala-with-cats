package example

import cats.Eval
import cats.data.IndexedStateT
import cats.data.State


object StateExample {

    val a: State[Int, String] = State[Int, String] { state =>
        (state, s"The state is $state")
    }

    val aRunned: Eval[(Int, String)] = a.run(10)

    val (aState: Int, aResult: String) = aRunned.value


    // Composing and transforming S

    val step1: State[Int, String] = State[Int, String] { num =>
        val ans = num + 1
        (ans, s"Result of step1: $ans")
    }

    val step2: State[Int, String] = State[Int, String] { num =>
        val ans = num * 2
        (ans, s"Result of step 2: $ans")
    }

    val step3: State[Int, String] = State[Int, String] { num =>
        val ans = num + 10
        (ans, s"Result of step 3: $ans")
    }

    val s99: IndexedStateT[Eval, Int, Int, (String, String, String)] = for {
        s01 <- step1
        s02 <- step2
        s03 <- step3
    } yield (s01, s02, s03)

    val (s01, r01) = s99.run(20).value

}
