package example

import cats.data.OptionT
//import cats.Monad
import cats.instances.list._        // for Monad
import cats.syntax.applicative._    // for pure

object ListOptionExample {
    type ListOption[A] = OptionT[List, A]

    val res1 = OptionT(List(Option(10)))
    val res2 = 10.pure[ListOption]


    val ex = for {
        x <- List(10)
        y <- List(32)
    } yield x + y

    val f0 = for {
        x <- res1
        y <- res2
    } yield x + y

    val f1 = for {
        x <- OptionT(List(Option(10), Option(15)))
        y <- res2
    } yield x + y

    val f2 = for {
        x <- OptionT(List(Option(10), None))
        y <- res2
    } yield x + y

}

object Something {
    case class Room(items: List[Item])
    case class Item(name: String)
    case class Player(items: List[Item])

    sealed trait Ops
    case object Exit
    case class PickUp(item: Item)
    case class Combine(item1: Item, item2: Item)

    case class GameState(player: Player, room: List[Room])

    val rooms = List(
        Room(List(Item("Key"), Item("Door"))),
        Room(List(Item("Winner cup")))
    )

    def exit(rooms: List[Room]): List[Room] =
        rooms.tail

    def exit(gameState: GameState): GameState =
        GameState(gameState.player, gameState.room.tail)

    def pickUp(item: Item, gameState: GameState): GameState =
        ???

}
