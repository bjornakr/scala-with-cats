package example

import cats.Foldable
import cats.instances.list._ // for Foldable
import cats.instances.option._ // for Foldable
object FoldableExamples {
    val ints = List(1, 2, 3)

    val a1: Int = Foldable[List].foldLeft(ints, 0)(_ + _) // = 6
    val a2: Int = Foldable[Option].foldLeft(Some(123), 10)(_ * _) // = Some(1230)



}
